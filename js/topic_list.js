//专题列表页的轮播图
var mySwiper1 = new Swiper(".swiper-container.big", {
  pagination: ".swiper-pagination",
  paginationClickable: true,
  autoplay: 5000,
  autoplayDisableOnInteraction: false,
  loop: true,
});
var mySwiper2 = new Swiper(".swiper-container.small", {
  slidesPerView: "auto",
  paginationClickable: true,
  autoplay: 5000,
  autoplayDisableOnInteraction: false,
});
$('.swiper-button-prev').on('click', function(e){
  e.preventDefault()
  mySwiper2.swipePrev()
})
$('.swiper-button-next').on('click', function(e){
  e.preventDefault()
  mySwiper2.swipeNext()
})


var mySwiper3 = new Swiper(".swiper-container.bottom", {
  paginationClickable: true,
  autoplay: 5000,
  autoplayDisableOnInteraction: false,
  slidesPerView: "auto",
  // loop:true
});
$('.swip-box3 .swiper-button-prev').on('click', function(e){
  e.preventDefault()
  mySwiper3.swipePrev()
})
$('.swip-box3 .swiper-button-next').on('click', function(e){
  e.preventDefault()
  mySwiper3.swipeNext()
})


//问题列表页的小轮播图
var mySwiper4 = new Swiper(".question-list .swiper-container", {
  pagination: ".question-list .swiper-pagination",
  paginationClickable: true,
  autoplay: 5000,
  autoplayDisableOnInteraction: false,
  loop: true,
});
$('.question-list .swiper-button-prev').on('click', function(e){
  e.preventDefault()
  mySwiper4.swipePrev()
})
$('.question-list .swiper-button-next').on('click', function(e){
  e.preventDefault()
  mySwiper3.swipeNext()
})
//问题详情页
$(".question-detail .left-items .left-item-d2 .items .item .ope").click(
  function () {
    if ($(this).find("img.hover").css("display") == "inline-block") {
      $(this).removeClass("clicked");
      $(this).find("img.hover").hide();
      $(this).find("img.default").show();
    } else {
      $(this).addClass("clicked");
      $(this).find("img.hover").css("display", "inline-block");
      $(this).find("img.default").hide();
    }
  }
);
var mySwiper5 = new Swiper(".question-detail .swiper-container", {
  pagination: ".question-detail  .swiper-pagination",
  paginationClickable: true,
  autoplay: 5000,
  autoplayDisableOnInteraction: false,
  loop: true,
  slidesPerView: "auto",
});

var showTitle_job = {
  init: function () {
    showTitle_job.showTitle();
  },
  showTitle: function () {
    $(function () {
      //问题列表页
      var tar1 = ".question-list .right-items .right-item1 ul li a,";
      var tar2 =
        ".question-list .right-items .right-item2 ul li a, .question-list .right-items .right-item3 ul li a,";
      var tar3 =
        ".question-list .left-items .left-item2 .item .box .tit .txt a,";
      var tar4 =
        ".question-detail .right-items .right-item-d3 .swiper-container .swiper-wrapper .swiper-slide .tit-box .tit a";
      var tar = tar1 + tar2 + tar3 + tar4;
      var tar_txt = $(tar);
      for (var i = 0; i < tar_txt.length; i++) {
        var tar = tar_txt[i];
        tar.setAttribute("title", tar_txt[i].innerText);
      }
    });
  },
};
showTitle_job.init();
