$(function () {
  $("nav ul li.arrow-down").hover(
    function () {
      $(this)
        .children("img")
        .attr("src", "/jyzx/img/icons/icon-arrow-down-rotate.png");
      if ($(this).find(".arrow-items").length > 0) {
        $(this).find(".arrow-items").stop(0, 0).slideDown();
      }
    },
    function () {
      $(this)
        .children("img")
        .attr("src", "/jyzx/img/icons/icon-arrow-down.png");
      $(this).find(".arrow-items").slideUp();
    }
  );
});
$("header .container .search .keywords a").click(function (e) {
  $("header .container .search .main-input form input.ipt-search").val(
    $(this).text()
  );
});

$(
  ".main.information-list .container .right .right-item2 .content .items .item, .main.information-list .container .right .right-item4 .content .items .item,.main.information-detail .container .right-items .rt3 .content .items .item"
).hover(function () {
  $(this).siblings().removeClass("hover");
  $(this).addClass("hover");
});

var showTitle = {
  init: function () {
    showTitle.showTitle();
  },
  showTitle: function () {
    $(function () {
      //主页banner
      var tar1 = ".banner .container .item ul li a,";
      //搜索结果页
      var tar2 = ".main.search-result .right .right-item1 ul li a,";
      var tar3 = " .main.search-result .right .right-item2 ul li a,";
      var tar4 =
        ".main.information-list .container .left .list .item .box .tit,";

      var tar5 = ".main.index .container .index1 .left ul li:first-child a,";
      var tar6 = ".tit a,.main a";

      var tar = tar1 + tar2 + tar3 + tar4 + tar5 + tar6;
      var tar_txt = $(tar);
      for (var i = 0; i < tar_txt.length; i++) {
        var tar = tar_txt[i];
        tar.setAttribute("title", tar_txt[i].innerText);
      }
    });
  },
};
showTitle.init();

var ieChange = {
  init: function () {
    if (ieChange.isIe()) {
      $('.main.information-list .container .left .list .item .box .txt').css('max-width','541px')
    }
  },
  isIe: function () {
    if (!!window.ActiveXObject || "ActiveXObject" in window) return true;
    else return false;
  },
};

ieChange.init();